#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 8080
#define BUFFER_SIZE 2048

typedef struct
{
    char *method;
    char *path;
    char *protocol;
    int num_headers;
    char **headers;
} Request;

int parse_request(const char *buffer, Request *request)
{
    char *line = strtok((char *)buffer, "\r\n");
    if (line == NULL)
        return -1;

    request->method = strtok(line, " ");
    request->path = strtok(NULL, " ");
    request->protocol = strtok(NULL, " ");

    if (request->method == NULL || request->path == NULL || request->protocol == NULL)
        return -1;

    request->num_headers = 0;
    line = strtok(NULL, "\r\n");
    printf("line: %s\n", line);
    while (line != NULL)
    {
        request->num_headers++;
        request = realloc(request, sizeof(Request) + request->num_headers + sizeof(char *));
        request->headers[request->num_headers - 1] = strdup(line);
        line = strtok(NULL, "\r\n");
    }

    return 0;
}

int main()
{
    char buffer[BUFFER_SIZE];
    char resp[] = "HTTP/1.0 200 OK\r\n"
                  "Server: webserver-c\r\n"
                  "Content-type: text/html\r\n\r\n"
                  "<html>hello, world</html>\r\n";

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        perror("webserver (socket)");
        return 1;
    }

    struct sockaddr_in host_addr;
    int host_addrlen = sizeof(host_addr);

    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(PORT);
    host_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    struct sockaddr_in client_addr;
    int client_addrlen = sizeof(client_addr);

    if (bind(sockfd, (struct sockaddr *)&host_addr, host_addrlen) != 0)
    {
        perror("webserver (bind)");
        return 1;
    }

    if (listen(sockfd, SOMAXCONN) != 0)
    {
        perror("webserver (listen)");
        return 1;
    }
    printf("webserver listening on http://127.0.0.1:%d\n", PORT);

    for (;;)
    {
        int newsockfd = accept(sockfd, (struct sockaddr *)&host_addr, (socklen_t *)&host_addrlen);
        if (newsockfd == -1)
        {
            perror("webserver (accept)");
            continue;
        }

        int sockn = getsockname(newsockfd, (struct sockaddr *)&client_addr, (socklen_t *)&client_addrlen);
        if (sockn == -1)
        {
            perror("webserver (getsockname)");
            continue;
        }

        int valread = read(newsockfd, buffer, BUFFER_SIZE);
        if (valread == -1)
        {
            perror("webserver (read)");
            continue;
        }
        printf("%s\n", buffer);

        Request *request = malloc(sizeof(Request));
        if (parse_request(buffer, request) == 0)
        {
            printf("Method: %s\n", request->method);
            printf("Path: %s\n", request->path);
            printf("Protocol: %s\n", request->protocol);
            printf("Headers:\n");
            for (int i = 0; i < request->num_headers; i++)
            {
                printf("%s\n", request->headers[i]);
            }
        }
        else
        {
            printf("Failed to parse request\n");
        }

        printf("[%s:%u]\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

        int valwrite = write(newsockfd, resp, strlen(resp));
        if (valwrite == -1)
        {
            perror("webserver (write)");
            continue;
        }

        close(newsockfd);
    }

    return 0;
}
